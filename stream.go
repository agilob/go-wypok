package go_wypok

import (
	"fmt"
)

// GetStreamEntries returns entries from Mikroblog.
// If user is logged in, blocked tags will be filtered.
func (wh *WykopHandler) GetStreamEntries(page uint) (entries []Entry, wypokError *WykopError) {
	responseBody := wh.sendPostRequestForBody(
		getStreamIndexUrl(wh, page),
	)

	wypokError = wh.getObjectFromJson(responseBody, &entries)
	return
}

// GetStreamLast6HoursHotEntries returns hot entries ("Gorące dyskusje") from Mikroblog which took place in last 6 hours
// If user is logged in, blocked tags will be filtered.
func (wh *WykopHandler) GetStreamLast6HoursHotEntries(page uint) (entries []Entry, wypokError *WykopError) {
	return wh.getStreamHotEntries(page, 6)
}

// GetStreamLast12HoursHotEntries returns hot entries ("Gorące dyskusje") from Mikroblog which took place in last 12 hours
// If user is logged in, blocked tags will be filtered.
func (wh *WykopHandler) GetStreamLast12HoursHotEntries(page uint) (entries []Entry, wypokError *WykopError) {
	return wh.getStreamHotEntries(page, 12)
}

// GetStreamLast24HoursHotEntries returns hot entries ("Gorące dyskusje") from Mikroblog which took place in last 24 hours
// If user is logged in, blocked tags will be filtered.
func (wh *WykopHandler) GetStreamLast24HoursHotEntries(page uint) (entries []Entry, wypokError *WykopError) {
	return wh.getStreamHotEntries(page, 24)
}

func (wh *WykopHandler) getStreamHotEntries(page, period uint) (entries []Entry, wypokError *WykopError) {
	responseBody := wh.sendPostRequestForBody(
		getStreamHotUrl(wh, page, period),
	)

	wypokError = wh.getObjectFromJson(responseBody, &entries)
	return
}

func getStreamIndexUrl(wh *WykopHandler, page uint) string {
	if wh.authResponse.Userkey != "" {
		return fmt.Sprintf(STREAM_INDEX_LOGGEDIN, wh.authResponse.Userkey, wh.appKey, page)
	}

	return fmt.Sprintf(STREAM_INDEX, wh.appKey, page)
}

func getStreamHotUrl(wh *WykopHandler, page, period uint) string {
	if wh.authResponse.Userkey != "" {
		return fmt.Sprintf(STREAM_HOT_LOGGEDIN, wh.authResponse.Userkey, wh.appKey, page, period)
	}

	return fmt.Sprintf(STREAM_HOT, wh.appKey, page, period)
}
